export class Card {
    public id:number = 0;
    public atk:number = 0;
    public def:number = 0;
    public price:number = 0;
    public name:string = "";
    public desc:string = "";
    public cost:number = 0;
    public img:string = "";
}
