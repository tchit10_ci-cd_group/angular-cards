#stage 1
FROM node:16.10 as node
WORKDIR /app
COPY ./cards-app .
RUN npm install
RUN npm run build --prod

#stage 2
FROM nginx:alpine
COPY --from=node /app/dist/cards-app /usr/share/nginx/html